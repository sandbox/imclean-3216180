/**
 * @file
 * JavaScript behaviors for dialogs.
 */

(function ($, Drupal, drupalSettings) {

  var initialised;
  
  function attachJs() {
    if (!initialised) {
      initialised = true;
      
      
      $('.btn-number').click(function (e) {
        e.preventDefault();

        fieldId = $(this).attr('data-field');
        console.log(fieldId);
        type = $(this).attr('data-type');
        var input = $("input#" + fieldId);
        var currentVal = parseInt(input.val());
        if (!isNaN(currentVal)) {
          if (type == 'minus') {

            if (currentVal > input.attr('min')) {
              input.val(currentVal - 1).change();
            }
          //  if (parseInt(input.val()) == input.attr('min')) {
            //  $(this).attr('disabled', true);
            //}

          } else if (type == 'plus') {

            //if (currentVal < input.attr('max')) {
              input.val(currentVal + 1).change();
            //}
            //if (parseInt(input.val()) == input.attr('max')) {
              //$(this).attr('disabled', true);
            //}

          }
        } else {
          input.val(0);
        }
      });
    }
  }

  
  /**
   * Attach JS..
   *
   * @type {Drupal~behavior}
   */
  Drupal.behaviors.bootstrapPlusMinus = {
    attach: function (context) {
      attachJs();
    }
  };

})(jQuery, Drupal, drupalSettings);
