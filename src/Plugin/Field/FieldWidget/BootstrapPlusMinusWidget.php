<?php

namespace Drupal\bootstrap_plus_minus\Plugin\Field\FieldWidget;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldWidget\NumberWidget;

/**
 * Extend number widget to provide plus/minus option to integer fields.
 *
 * @FieldWidget(
 *   id = "bootstrap_plus_minus",
 *   label = @Translation("Bootstrap Plus/Minus"),
 *   field_types = {
 *     "integer",
 *     "decimal"
 *   },
 *   multiple_values = true
 * )
 */
class BootstrapPlusMinusWidget extends NumberWidget {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $value = parent::formElement($items, $delta, $element, $form, $form_state);
    $element = $value['value'];

    // Enforce integers for now.
    if ($this->fieldDefinition->getType() === 'decimal') {
      $element['#step'] = 1;
    }

    // Flag the element for the theme layer.
    $element['#attributes']['class'][] = 'bpm-input';
    //$element['#theme'] = 'input__form_control__number__bootstrap_plus_minus';
    $element['#theme'] = 'bootstrap_plus_minus';
    $element['#attached']['library'][] = 'bootstrap_plus_minus/bootstrap_plus_minus';
   
    // Everything else is done in the theme for now. 
    // @todo Move theme logic into the module.

    return ['value' => $element];
  }

  
}
